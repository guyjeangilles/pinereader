import typing

from GDEW042T2 import GDEW042T2


if typing.TYPE_CHECKING:
    from PIL import Image


class Display:
    """Abstraction layer for the display."""
    
    def __init__(self):
        self._display = GDEW042T2()
        self.height = self._display.height
        self.width = self._display.width

    def display_image(self, image: "Image") -> None:
        """Wrapper for setting the display."""
        self._display.display_image(image)
    
    def close(self) -> None:
        """Wrapper to shutdown display."""
        self._display.close()

    def size(self) -> tuple[int, int]:
        """Hieght and width in pixels."""
        return self._display.height, self._display.width
