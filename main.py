import logging
import typing

from bookpicker import BookPicker
from bookviewer import BookViewer
from buttonmonitor import ButtonMonitor
from display import Display


if typing.TYPE_CHECKING:
    from PIL import Image


class UIManager:
    """Handles switching between UI menus."""

    def __init__(self):
        self._disp = Display()
        self._picker = BookPicker()
        self._monitor = ButtonMonitor()
        self._viewer = BookViewer()
        self._image_maker = None
        self._old_image = None

        self._up_callback = lambda *args: None
        self._down_callback = lambda *args: None
        self._right_callback = lambda *args: None
        self._left_callback = lambda *args: None
        self._select_callback = lambda *args: None

        self.show_books()

    def run(self) -> None:
        """Main event loop."""
        image = self._picker.make_ui(self._disp.size())
        self.display_image(image)

        while 1:
            image = self._image_maker(self._disp.size())
            if image != self._old_image:
                self.display_image(image)

            if self._monitor.up_pressed():
                self._up_callback()
            elif self._monitor.right_pressed():
                self._right_callback()
            elif self._monitor.down_pressed():
                self._down_callback()
            elif self._monitor.left_pressed():
                self._left_callback()
            elif self._monitor.select_pressed():
                self._select_callback()

    def select_book(self) -> None:
        """Prepares UI elements to render book pages."""
        self._viewer.open_book(self._picker.bookpath())
        self._viewer.set_page_index(0)
        self._image_maker = self._viewer.load_page

        self._up_callback = lambda *args: None
        self._down_callback = lambda *args: None
        self._left_callback = self._viewer.decrement_page_index
        self._right_callback = self._viewer.increment_page_index
        self._select_callback = self.show_books

    def show_books(self) -> None:
        """Prepare UI elements to show bookshelf."""
        self._image_maker = self._picker.make_ui

        self._up_callback = self._picker.decrement_book_index
        self._down_callback = self._picker.increment_book_index
        self._right_callback = lambda *args: None
        self._left_callback = lambda *args: None
        self._select_callback = self.select_book


    def display_image(self, image: "Image") -> None:
        """Ensures screen is saved after refreshing."""
        self._disp.display_image(image)
        self._old_image = image

    def close(self) -> None:
        """Release all resources."""
        self._disp.close()
        self._monitor.close()


if __name__ == "__main__":
    try:
        ui = UIManager()
        while 1:
            ui.run()
        ui.close()
    except (Exception, KeyboardInterrupt) as e:
        logging.error(e)
        ui.close()
