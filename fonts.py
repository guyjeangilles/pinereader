import logging
import os

from PIL import ImageFont


class FontMaker:
    """Tools to make a displayable font."""

    def __init__(self):
        self._font_path = "/usr/share/fonts/truetype/freefont/FreeMono.ttf"
        if not os.path.isfile(self._font_path):
            logging.error(f"Did not find a font at {self._font_path}")

    def font(self, size: int = 11) -> ImageFont:
        """Create a displayable font."""
        return ImageFont.truetype(self._font_path, size)


if __name__ == "__main__":
    import time
    from display import Display
    from PIL import Image, ImageDraw
    try:
        disp = Display()
        image = Image.new("1", disp.size(), 255)
        draw = ImageDraw.Draw(image)
        fontmaker = FontMaker()

        draw.text((0, 0), "hello world", font=fontmaker.font(), fill=0)
        draw.text((0, disp.height / 3), "hello world", font=fontmaker.font(size=24), fill=0)
        draw.text((0, (2 * disp.height) / 3), "hello world", font=fontmaker.font(size=36), fill=0)

        disp.display_image(image)
        time.sleep(2)
        disp.close()
    except (Exception, KeyboardInterrupt):
        disp.close()
