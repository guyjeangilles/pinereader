
# configure python virtual environment
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip
sudo apt-get install -y python-dev-is-python3

# TODO enable spi

# install normal python packages
pip install spidev
pip install Pillow
pip install pymupdf

# install RPi.GPIO-Pine64
git clone https://github.com/swkim01/RPi.GPIO-PineA64.git
cd RPi.GPIO-PineA64
sudo ../../venv/bin/python setup.py install # this will fail if gcc is not installed

# install Waveshare epaper driver
cd ..
git clone https://github.com/waveshare/e-Paper.git
cp -r e-Paper/RaspberryPi_JetsonNano/python/lib/waveshare_epd/ epaper/
sudo rm -r e-Paper/
# TODO patch to always detect Raspberry Pi

# make books directory
mkdir ~/Bookshelf
