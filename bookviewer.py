import fitz
import logging
import os
from PIL import Image


class BookViewer:
    """Book viewer UI."""

    def __init__(self):
        self._doc = None
        self._page_idx = 0

    def load_page(self, size: tuple[int, int]) -> Image:
        """Render a book page."""
        if self._doc is None:
            logging.error("Call BookViewer.open_book before calling BookViewer.load_page.")

        height, width = size
        self._doc.layout(rect=self._doc[0].rect, width=width, height=height, fontsize=16)
        page_pix = self._doc[self._page_idx].get_pixmap()
        image = Image.frombytes("RGB", [page_pix.width, page_pix.height], page_pix.samples)
        return image.resize(size)

    def open_book(self, bookpath: str) -> None:
        """Open a book on the filesystem."""
        if os.path.isfile(bookpath):
            self._doc = fitz.open(bookpath)
        else:
            logging.error(f"{bookpath} is not a valid filepath.")
    
    def increment_page_index(self) -> None:
        if self._doc is None:
            logging.error("Call BookViewer.open_book before calling BookViewer.increment_page_index.")
            return
        
        idx = self._page_idx + 1
        page_count = self._doc.page_count
        if idx > page_count - 1:
            logging.warning("Did not change page to {idx + 1}. Document has {page_count} pages.")
        else:
            self._page_idx = idx

    def decrement_page_index(self) -> None:
        if self._doc is None:
            logging.error("Call BookViewer.open_book before calling BookViewer.decrement_page_index.")
            return

        idx = self._page_idx - 1
        if idx < 0:
            logging.warning("Did not change page to negative number.")
        else:
            self._page_idx = idx

    def set_page_index(self, index: int) -> None:
        if self._doc is None:
            logging.error("Call BookViewer.open_book before calling BookViewer.set_page_index.")
            return
        
        page_count = self._doc.page_count
        if 0 <= index < page_count - 1:
            self._page_idx = index
        else:
            logging.error(f"Cannot open page {index + 1} of document with {page_count} page(s).")

if __name__ == "__main__":
    import time
    from pathlib import Path
    from display import Display

    disp = Display()
    viewer = BookViewer()
    viewer.open_book(str(Path.home()) + "/Bookshelf/fullmetal-alchemist.cbz")
    try:
        disp.display_image(viewer.load_page(disp.size()))
        time.sleep(2)
        disp.close()
    except (Exception, KeyboardInterrupt):
        disp.close()
