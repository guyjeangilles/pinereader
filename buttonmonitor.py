import time
import RPi.GPIO as GPIO


class ButtonMonitor:
    """Listen for button presses."""

    def __init__(self):        
        self._up_pin = 27
        self._down_pin = 22
        self._right_pin = 23
        self._left_pin = 5
        self._select_pin = 26
        self._buttons = [self._up_pin, self._down_pin, self._right_pin, self._left_pin, self._select_pin]
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self._buttons, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        self._up_pin_state = False
        self._last_up_pin_state = False
        self._down_pin_state = False
        self._last_down_pin_state = False
        self._right_pin_state = False
        self._last_right_pin_state = False
        self._left_pin_state = False
        self._last_left_pin_state = False
        self._select_pin_state = False
        self._last_select_pin_state = False

        self._debounce_time = 50000  # in nanoseconds
        self._last_debounce_time = 0

    def up_pressed(self) -> bool:
        """If up was pressed."""
        if time.time_ns() - self._last_debounce_time  > self._debounce_time:
            self._last_debounce_time = time.time_ns()
            self._up_pin_state = GPIO.input(self._up_pin)
            pressed = self._up_pin_state and not self._last_up_pin_state
            self._last_up_pin_state = self._up_pin_state
            return pressed
        return False

    def right_pressed(self) -> bool:
        """If right was pressed."""
        if time.time_ns() - self._last_debounce_time  > self._debounce_time:
            self._last_debounce_time = time.time_ns()
            self._right_pin_state = GPIO.input(self._right_pin)
            pressed = self._right_pin_state and not self._last_right_pin_state
            self._last_right_pin_state = self._right_pin_state
            return pressed
        return False

    def down_pressed(self) -> bool:
        """If down was pressed."""
        if time.time_ns() - self._last_debounce_time  > self._debounce_time:
            self._last_debounce_time = time.time_ns()
            self._down_pin_state = GPIO.input(self._down_pin)
            pressed = self._down_pin_state and not self._last_down_pin_state
            self._last_down_pin_state = self._down_pin_state
            return pressed
        return False

    def left_pressed(self) -> bool:
        """If left was pressed."""
        if time.time_ns() - self._last_debounce_time  > self._debounce_time:
            self._last_debounce_time = time.time_ns()
            self._left_pin_state = GPIO.input(self._left_pin)
            pressed = self._left_pin_state and not self._last_left_pin_state
            self._last_left_pin_state = self._left_pin_state
            return pressed
        return False

    def select_pressed(self) -> bool:
        """If select was pressed."""
        if time.time_ns() - self._last_debounce_time  > self._debounce_time:
            self._last_debounce_time = time.time_ns()
            self._select_pin_state = GPIO.input(self._select_pin)
            pressed = self._select_pin_state and not self._last_select_pin_state
            self._last_select_pin_state = self._select_pin_state
            return pressed
        return False

    def close(self) -> None:
        """Clean up gpios."""
        for button in self._buttons:
            GPIO.remove_event_detect(button)


if __name__ == "__main__":
    monitor = ButtonMonitor()
    try:
        while 1:
            if monitor.up_pressed():
                print("up.")
            elif monitor.right_pressed():
                print("right.")
            elif monitor.left_pressed():
                print("left.")
            elif monitor.down_pressed():
                print("down.")
            elif monitor.select_pressed():
                print("select.")
        monitor.close()
    except (Exception, KeyboardInterrupt):
        monitor.close()
